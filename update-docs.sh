#!/bin/bash
#####################################################################################################
#                               COMBINED DOXYGEN DOCUMENTATION GENERATOR                            #
#                                                                                                   #
# This script will run Doxygen (http://doxygen.nl) on files referenced as aliases in the local      #
# ./doxygen-files/ directory. This directory should contain symlinks to "Doxyfile" instances (which #
# each need to be named "Doxyfile" -case-sensitive). The Doxyfile needs to generate an "html" dir   #
# in the same directory as the Doxyfile. This script will remove the generated html directory, and  #
# place it in the local ./bmlt-doc/ directory, named with the same name given to the symlink.       #
#                                                                                                   #
# An index.html file will be copied into the ./bmlt-doc/ directory, and will link into the various  #
# generated directories.                                                                            #
#                                                                                                   #
#####################################################################################################

########################################
# This is a function that will really nail down where this script is located.
# From here: http://www.ostricher.com/2014/10/the-right-way-to-get-the-directory-of-a-bash-script/
get_script_dir () {
     SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     echo "$DIR"
}

########################################

# So we can leave things as they were.
old_dir="$PWD"

# Find out exactly where we are. I use a complicated, but reliable method.
script_dir="$(get_script_dir)"

# If there was already an output dir, we get rid of it.
rm -drf "${script_dir}/bmlt-doc"

# Create a new output dir.
mkdir "${script_dir}/bmlt-doc"

# Copy the index file and the iframe contents into it.
cp "${script_dir}/index.html" "${script_dir}/bmlt-doc/index.html"
cp "${script_dir}/selectdocs.html" "${script_dir}/bmlt-doc/selectdocs.html"

# Read in the list of files in the symlink dir.
doxyfiles="${script_dir}/doxygen-files/*"

# Run a generator, followed by a move, for each of these files.
for doxyfile in $doxyfiles
do
    # Get the Actual, real location of the Doxyfile (Absolute POSIX).
    actual_loc=`readlink "${doxyfile}"`
    # Get the containing directory.
    actual_dir=`dirname "${actual_loc}"`
    # Get just the filename.
    file_name=`basename "${doxyfile}"`
    echo "Generating ${file_name}."
    cd "${actual_dir}"
    # Doxygen is very chatty. We'll assume it's all OK. This is not a debug or test script.
    /Applications/Doxygen.app/Contents/Resources/doxygen Doxyfile >/dev/null * 2>/dev/null
    # Move the generated html directory to the aggregate dir.
    mv "html" "${script_dir}/bmlt-doc/${file_name}"
done

# Leave things like a Boy Scout...
cd "${old_dir}"